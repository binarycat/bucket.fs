# a library for simple persitant binary data storage

if you define `VARIABLE x`, the value of `x` will be erased as soon as you exit the program.
this library to provide convenient and simple binary data storge that can easily be slotted into small programs and games.

## the bucket datatype

a bucket is the central datatype of this libary.
implementation-wise, it corrosponds to a file.  it is recommended that you create this file with the `truncate` unix command.

## grains

if you add more grains, they should be added at the end.

a grain refers to some slice of a bucket.

these are allocated simlary to how ALLOT allocates memory from data-space

they are accessed like normal memory variables.  they will not be saved untill SAVE-BUCKET is ran on their containing bucket.
