\ example program: stateful fibbonaci sequence generator
require bucket.fs

\ open existing bucket file or create a new one
s" fib_bucket.bin" r/w bin open-file [IF]
  s" fib_bucket.bin" r/w bin create-file throw
[THEN]
3 cells bucket: fib-bucket

fib-bucket
cell grain: index
cell grain: prev
cell grain: this
load-bucket

: init
  2 index !
  1 prev !
  1 this ! ;

: step 
  index @ 0= if init then
  1 index +!
  prev @ this @ +
  this @ prev !
  this !
  ." fib(" index @ 0 u.r ." ) = " this ? cr ;

step

fib-bucket save-bucket

bye
