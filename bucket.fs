\ simple forth library for storing persistant binary data
\ intended for small retro games and such
\ data in a "bucket" is buffered in memory so it can be modified with
\ regular data-access words

begin-structure /bucket
field: bucket-file
field: bucket-size
\ how much of the bucket has been assigned to variables.  analogous to HERE
field: bucket-used
0 +field bucket-buf
end-structure

: bucket: ( fileid size "name" -- )
  tuck create
  swap , , 0 ,
  chars here over allot swap 0 fill ;

\ common code for reading/writing
: _bucket ( bucket -- addr u fileid )
  dup bucket-file @ 0 0 rot
  reposition-file throw 
  dup bucket-buf
  swap dup bucket-size @
  swap bucket-file @ ;

: load-bucket ( bucket -- )
  _bucket
  read-file throw drop ;

: save-bucket ( bucket -- )
  _bucket
  write-file throw ;

\ skip over n bytes of a bucket
: bucket-skip ( n bucket -- )
  tuck bucket-used +!
  dup bucket-size @ swap bucket-used @ < abort" bucket is too small!" ;

\ assign n bytes of a bucket
: bucket-scoop ( n bucket -- addr )
  dup bucket-buf
  over bucket-used @ + >r
  bucket-skip r> ;

\ assign n bytes of a bucket to a word
\ the stack effect is such that they can easily be chained one after another
: grain: ( bucket n -- bucket )
  over bucket-scoop constant ;

